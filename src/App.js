import {useEffect, useState} from 'react';

import {ModeChouse} from "./components/ModeChouse/ModeChouse";
import {FieldsBlock} from "./components/FieldsBlock/FieldsBlock";

import './App.css';


function App() {
    const [mode, setMode] = useState([])
    /** храним массив с ответом с сервера */
    const [actualMode, setActualMode] = useState('')
    /** храним преобразований уровень сложности */
    const [start, setStart] = useState('')  /** храним уровень сложности для отправки по клику */

    useEffect(() => {
        fetch('http://demo1030918.mockable.io/')
            .then((response) => response.json())
            .then((data) => {
                setMode(Object.entries(data)) /** преобразовываем в массив для облегчения итерации */
            });
    }, []) /** зависимость - единоразово при рендеринге компонента */

    return (
        <div className="App">
            <div className="start-block">
                <ModeChouse mode={mode} handleChangeMode={setActualMode}/>
                <button className='inline-block btn' onClick={() => setStart(actualMode)}>START</button>
            </div>
            <FieldsBlock fieldSize={+start}/>
        </div>
    );
}

export default App;
