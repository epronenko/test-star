import {useState, useEffect} from 'react'

export const Td = ({makeWhite, callBackSetState}) => {
    const [hovered, setHovered] = useState(null)
    /** у каждой ячейки свой стейт, который от которого зависит цвет ячеки */
    useEffect(() => {
        setHovered(null) /** сбрасываем всем ячейкам стейт и как следствие класс */
    }, [makeWhite]) /** makeWhite === fieldSize потому при смене сложности обновляем поле */

    const changeColor = () => {
            setHovered((prevState) => !prevState)
            if (!hovered){callBackSetState()} /** если ячейка окрашивается в белый, игнорируем ее  везде */
        }
    return <td className={hovered ? 'square blue' : 'square'} onMouseEnter={changeColor}/>
}

