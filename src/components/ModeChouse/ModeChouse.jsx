import React from 'react';

export const ModeChouse = ({mode, handleChangeMode}) => {
    const optionArr = mode.map(item => <option key={item} value={item[1]['field']}>{item[0]}</option>)
    /** создаем список option в зависимости от присланных с сервера данных*/

    return (
        <>
            <div className='choose-mode inline-block'>
                <select onChange={(e) => handleChangeMode(e.target.value)}>
                    <option disabled="disabled" selected={true} >выберите уровень сложности</option>
                    {optionArr}
                </select>
            </div>
        </>
    );
};

/** 10 строка - достаем выбранное значение и отпрвляем в проброшенную функцию setActualMode
 * 11 строка - автовыбор дефолтного неактивного option
 * */