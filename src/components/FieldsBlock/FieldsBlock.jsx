import {useState, useEffect} from 'react';
import {Td} from "../Td/Td";

export const FieldsBlock = ({fieldSize}) => {
    const [state, setState] = useState([])
    /** массив координат ячеек */
    const [show, setShow] = useState([])
    /** "логи" */

    const templateArr = new Array(fieldSize).fill(null)
    /** создаем массив заданной длинны, присланной сверху */

    const addRow = (rowNumber) => {
        /**создаем функцию которая меняет стейт через вызов сетстейт в другом месте*/
        setState((prevState) => [...prevState, rowNumber])
    }

    useEffect(() => {
        addLog()
    }, [state])
    useEffect(() => {
        setShow([]) /** обнуляем список "логов" при смене сложности */
    }, [fieldSize])

    const fieldsBlock = templateArr.map((tr, indexTr) => {
        /** создаем нужное кол-во строк */
        const tdElements = templateArr.map((td, indexTd) => { /** в каждую строку столько же ячеек (компонентов) */
            return <Td makeWhite={fieldSize}
                       callBackSetState={() => addRow(indexTr * fieldSize + indexTd)} /> /** отдельный компонент для добавления своего стейта */
            /**
             * makeWhite - обнуление поля при смене сложности
             * addRow - пробрасываем setState в коллбэке
             * */
        })
        return <tr className='square'>{tdElements}</tr>
    })

    const lastElement = state[state.length - 1]
    /** находим последнюю закрашенную ячейку */

    const addLog = () => {
        if (lastElement + 1) {
            /** отсекаем undefined */
            setShow([...show, <div
                className='log-item'>row {Math.floor(lastElement / fieldSize) + 1} col {lastElement % fieldSize + 1}</div>]) /** последнюю ячейку сразу сохраняем с разметкой */
        }
    }

    return (<div className='flex'>
            <div>
                <table className='table'>
                    <tbody>
                    {fieldsBlock}
                    </tbody>
                </table>
            </div>
            <div className='col flex text-block'>
                <div className='log-title'>Hover squares</div>
                {show}</div>
        </div>
    );
};